const express = require('express');
const app = express();
const fs = require('fs');
const util = require('util');
const readdirPromise = util.promisify(fs.readdir);
const path = require('path');
const morgan = require('morgan');

app.use(express.json());
app.use(morgan('common'));

const baseDir = path.join(__dirname, '/files');

getFiles = async () => {
  const currentFiles = await readdirPromise(baseDir);
  return currentFiles.filter((file) => file !== '.gitkeep');
};

app.get('/api/files', async (req, res) => {
  try {
    const currentFiles = await getFiles();
    if (currentFiles.length >= 0) {
      res.status(200).json({
        message: 'Success',
        files: currentFiles
      });
    } else {
      res.status(400).json({ message: 'Client error' });
    }
  } catch (e) {
    res.status(500).json({ message: 'Server error' });
  }
});

app.post('/api/files', (req, res) => {
  try {
    const { filename, content } = req.body;
    const regexp = /\.(?:log|txt|json|yaml|xml|js)$/g;

    if (filename && content) {
      if (regexp.test(filename)) {
        fs.writeFileSync(path.join(baseDir, filename), content);

        res.status(200).json({ message: 'File created successfully' });
      } else {
        res.status(400).json({ message: 'Wrong extension' });
      }
    } else {
      if (!content) {
        res.status(400).json({ message: "Please specify 'content' parameter" });
      } else if (!filename) {
        res
          .status(400)
          .json({ message: "Please specify 'filename' parameter" });
      }
    }
  } catch (e) {
    res.status(500).json({ message: 'Server error' });
  }
});

app.get('/api/files/:filename', async (req, res) => {
  try {
    const filename = req.params.filename;
    const currentFiles = await getFiles();
    if (filename) {
      const result = currentFiles.find((file) => file === filename);
      if (result) {
        const uploadedDate = fs.statSync(path.join(baseDir, result)).birthtime;
        const content = fs.readFileSync(path.join(baseDir, result)).toString();
        const extension = path.extname(result).split('.')[1];

        res.status(200).json({
          message: 'Success',
          filename,
          content,
          extension,
          uploadedDate
        });
      } else {
        res
          .status(400)
          .json({ message: `No file with ${filename} filename found` });
      }
    }
  } catch (e) {
    res.status(500).json({ message: 'Server error' });
  }
});

app.delete('/api/files/:filename', async (req, res) => {
  try {
    const filename = req.params.filename;
    const currentFiles = await getFiles();
    if (filename) {
      const result = currentFiles.find((file) => file === filename);

      if (result) {
        fs.unlinkSync(path.join(baseDir, result));
        res.status(200).json({ message: 'File deleted successfully' });
      } else {
        res
          .status(400)
          .json({ message: `No file with ${filename} filename found` });
      }
    }
  } catch (e) {
    res.status(500).json({ message: 'Server error' });
  }
});

app.put('/api/files/:filename', async (req, res) => {
  try {
    const filename = req.params.filename;
    const { filename: newFilename, content: newContent } = req.body;
    const currentFiles = await getFiles();
    if (filename) {
      const result = currentFiles.find((file) => file === filename);
      if (result) {
        if (newContent) {
          const content = fs
            .readFileSync(path.join(baseDir, result))
            .toString();
          if (content !== newContent) {
            fs.writeFileSync(path.join(baseDir, result), newContent);
          }
        }

        if (newFilename) {
          if (filename !== newFilename) {
            fs.renameSync(
              path.join(baseDir, filename),
              path.join(baseDir, newFilename)
            );
          }
        }

        if (!newFilename && !newContent) {
          res
            .status(400)
            .json({ message: 'Enter new filename or new content' });
        }
        res.status(200).json({ message: 'File updated successfully' });
      } else {
        res
          .status(400)
          .json({ message: `No file with ${filename} filename found` });
      }
    }
  } catch (e) {
    res.status(500).json({ message: 'Server error' });
  }
});

app.listen(8080);
